﻿using RimWorld;
using UnityEngine;
using Verse;

namespace AncientDungeons
{
    public class AncientDungeonsSettings : ModSettings
    {
        public float dungeonSpawnProbability = 0.1f;
       
        public override void ExposeData()
        {
            Scribe_Values.Look(ref dungeonSpawnProbability, "dungeonSpawnProbability", 0.1f);
            base.ExposeData();
        }
    }

    public class AncientDungeons : Mod
    {
        AncientDungeonsSettings settings;

        public AncientDungeons(ModContentPack content) : base(content) 
        {
            settings = GetSettings<AncientDungeonsSettings>();
            Log.Message($"Hello World from AncientDungeons! Dungeon spawn probability is {settings.dungeonSpawnProbability}");
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard listingStandard = new Listing_Standard();
            listingStandard.Begin(inRect);
            listingStandard.Gap(16f);
            listingStandard.Label("Ancient Dungeons frequency: " + System.Math.Round(settings.dungeonSpawnProbability * 100f, 1) + "%");
            settings.dungeonSpawnProbability = listingStandard.Slider(settings.dungeonSpawnProbability, 0.0f, 1.0f);
            bool resetToDefault = listingStandard.ButtonText("Reset to Default (10%)");
            if (resetToDefault)
            {
                settings.dungeonSpawnProbability = 0.1f;
            }
            listingStandard.End();
            Log.Message($"Dungeon spawn probability is set to {settings.dungeonSpawnProbability}");
            base.DoSettingsWindowContents(inRect);
        }

        public override string SettingsCategory()
        {
            return "AncientDungeons";
        }
    }
}
